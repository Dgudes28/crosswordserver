﻿using MongoDB.Bson.Serialization.Serializers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CrosswordsApi.Models;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.IO;

namespace CrosswordsApi
{
    public class myDefinitionListSerializer : SerializerBase<Definition[]>
    {
        public override Definition[] Deserialize(BsonDeserializationContext context, BsonDeserializationArgs args)
        {
            List<Definition> definitionList = new List<Definition>();

            context.Reader.ReadStartArray();
            while (true)
            {
                try
                {
                    context.Reader.ReadStartDocument();
                }
                catch (Exception exp)
                {
                    context.Reader.ReadEndArray();
                    break;
                }
                var Question = context.Reader.ReadString();
                var Position = context.Reader.ReadInt32();
                var Answer = context.Reader.ReadString();
                var DefNum = context.Reader.ReadInt32();

                definitionList.Add(new Definition()
                {
                    Question = Question,
                    Answer = Answer,
                    Position = Position,
                    DefNum = DefNum
                });
                context.Reader.ReadEndDocument();
            }
            return definitionList.ToArray();
        }


        public override void Serialize(BsonSerializationContext context, BsonSerializationArgs args, Definition[] definitionList)
        {
            context.Writer.WriteStartArray();
            foreach (Definition definition in definitionList)
            {
                context.Writer.WriteStartDocument();
                context.Writer.WriteName("Question");
                context.Writer.WriteString(definition.Question);
                context.Writer.WriteName("Position");
                context.Writer.WriteInt32(definition.Position);
                context.Writer.WriteName("Answer");
                context.Writer.WriteString(definition.Answer);
                context.Writer.WriteName("DefNum");
                context.Writer.WriteInt32(definition.DefNum);
                context.Writer.WriteEndDocument();
            }
            context.Writer.WriteEndArray();
        }
    }
}
