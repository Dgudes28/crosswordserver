﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Driver;
using CrosswordsApi.Models;
using System.Text.Json;
using MongoDB.Bson;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Mvc;

namespace CrosswordsApi.Dal
{
    public interface ICrosswordDal
    {
        Task<Crossword> GetCrosswordById(string id);
    }
    public class CrosswordDal: ICrosswordDal
    {
        private readonly IMongoCollection<Crossword> _crosswordDal;

        public CrosswordDal(ICrosswordsDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _crosswordDal = database.GetCollection<Crossword>(settings.CrosswordsCollectionName);
        }

        public async Task<Crossword> GetCrosswordById(string id)
        {
            var filter = Builders<Crossword>.Filter.Eq("_id",ObjectId.Parse(id));
            Crossword crosswordDocument = await _crosswordDal.Find<Crossword>(filter).FirstOrDefaultAsync();
            return crosswordDocument;
        }

        public async void SaveCrossword(Crossword crossword)
        {
            await _crosswordDal.InsertOneAsync(crossword);
         }


        /// <summary>
        /// examples below my writing above
        /// </summary>
        /// <returns></returns>
        //public List<Crossword> Get() =>
        //    _crosswordDal.Find(crossword => true).ToList();

        //public Crossword Get(string id) =>
        //    _crosswordDal.Find<Crossword>(crossword => crossword.Id == id).FirstOrDefault();

        //public Crossword Create(Crossword crossword)
        //{
        //    _crosswordDal.InsertOne(crossword);
        //    return crossword;
        //}

        //public void Update(string id, Crossword crosswordIn) =>
        //    _crosswordDal.ReplaceOne(crossword => crossword.Id == id, crosswordIn);

        //public void Remove(Crossword crosswordIn) =>
        //    _crosswordDal.DeleteOne(crossword => crossword.Id == crosswordIn.Id);

        //public void Remove(string id) =>
        //    _crosswordDal.DeleteOne(crossword => crossword.Id == id);
    }
}