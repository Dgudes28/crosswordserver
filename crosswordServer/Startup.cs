﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CrosswordsApi.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using CrosswordsApi.Dal;

namespace CrosswordsApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        readonly string MyAllowSpecificOrigins = "_myAllowSpecificOrigins";
        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();
            services.AddMvc();
            services.Configure<CrosswordsDatabaseSettings>(
             Configuration.GetSection(nameof(CrosswordsDatabaseSettings)));

            services.AddSingleton<ICrosswordsDatabaseSettings>(sp =>
                sp.GetRequiredService<IOptions<CrosswordsDatabaseSettings>>().Value);

            services.AddSingleton<CrosswordDal>();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors(
            options => options.WithOrigins("http://localhost:4200").AllowAnyHeader()
        );
            app.UseMvc();

        }
    }
}
