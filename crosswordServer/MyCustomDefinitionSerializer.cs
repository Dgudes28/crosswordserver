﻿using MongoDB.Bson.Serialization.Serializers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CrosswordsApi.Models;
using MongoDB.Bson.Serialization;

namespace CrosswordsApi
{
    public class MyCustomDefinitionSerializer : SerializerBase<Definition>, IBsonDocumentSerializer
    {
        public bool TryGetMemberSerializationInfo(string memberName, out BsonSerializationInfo serializationInfo)
        {
            switch (memberName)
            {
                case "Question":
                    serializationInfo = new BsonSerializationInfo("Question", new ObjectIdSerializer(), typeof(string));
                    return true;
                case "Answer":
                    serializationInfo = new BsonSerializationInfo("Answer", new StringSerializer(), typeof(string));
                    return true;
                case "Position":
                    serializationInfo = new BsonSerializationInfo("Position", new StringSerializer(), typeof(int));
                    return true;
                default:
                    serializationInfo = null;
                    return false;
            }
        }
    }
}

