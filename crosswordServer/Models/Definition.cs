﻿using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace CrosswordsApi.Models
{
    public class Definition
    {
        public string Question { get; set; }
        public string Answer { get; set; }
        public int Position { get; set; }
        public int DefNum { get; set; }
    }

    //[JsonConverter(typeof(StringEnumConverter))]
    //public enum DefinitionDirection
    //{
    //    [EnumMember(Value = "Horizontal")]
    //    Horizontal,
    //    [EnumMember(Value = "Vertical")]
    //    Vertical
    //}
}
