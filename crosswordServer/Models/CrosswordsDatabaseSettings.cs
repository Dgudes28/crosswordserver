﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CrosswordsApi.Models
{
    public class CrosswordsDatabaseSettings : ICrosswordsDatabaseSettings
    {
        public string CrosswordsCollectionName { get; set; }
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }

    public interface ICrosswordsDatabaseSettings
    {
        string CrosswordsCollectionName { get; set; }
        string ConnectionString { get; set; }
        string DatabaseName { get; set; }
    }
}