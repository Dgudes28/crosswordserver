﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CrosswordsApi.Models
{
    public class Crossword
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("Name")]
        public string Name { get; set; }

        [BsonElement("NumberOfRows")]
        public int NumberOfRows { get; set; }

        [BsonElement("NumberOfCols")]
        public int NumberOfCols { get; set; }

        [BsonSerializer(typeof(myDefinitionListSerializer))]
        public Definition[] HorizontalDefinitions { get; set; }

        [BsonSerializer(typeof(myDefinitionListSerializer))]
        public Definition[] VerticalDefinitions { get; set; }
    }
}
