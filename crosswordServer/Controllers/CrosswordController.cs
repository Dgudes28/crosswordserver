﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using CrosswordsApi.Models;
using CrosswordsApi.Dal;
using MongoDB.Bson;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Cors;

namespace CrosswordsApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Crossword/")]
    public class CrosswordController : Controller
    {

        private CrosswordDal _crosswordDal;

        public CrosswordController(CrosswordDal crossword)
        {
            this._crosswordDal = crossword;
        }

        // GET: api/Crossword/Crossword
        //[Route("GetCrosswordId")]
        //[HttpGet]
        //public Task<string> GetCrosswordId()
        //{
        //    return _crosswordDal.GetCrosswordId();
        //}

        // GET: api/Crossword/5
        [HttpGet("GetCrosswordById/{id}")]
        public Task<Crossword> GetCrosswordById(string id)
        {
            return _crosswordDal.GetCrosswordById(id);
        }


        // POST: api/SaveCrossword
        [Route("SaveCrossword")]
        [HttpPost]
        public void SaveCrossword([FromBody]Crossword crossword)
        {
            //Crossword crossword = data["crossword"].ToObject<Crossword>();
            _crosswordDal.SaveCrossword(crossword);
        }

        // PUT: api/Crossword/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
